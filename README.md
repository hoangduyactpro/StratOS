# StratOS ROM (AmazFit sport watch 2 / AmazFit Stratos)

Here you can find all the sources for the apps translations of my StratOS ROM! 

## StratOS installation

My ROM is available on my website in Italian (see http://nicolasagliano.com/) or in English on XDA: (see https://forum.xda-developers.com/smartwatch/amazfit/rom-stratos-amazfit-sport-watch-2-t3746334)

## Goals

This project is meant to provide sources for building custom ROMs for the above mentioned smartwatches (AmazFit sport watch 2 / AmazFit Stratos).

- Multilanguage support, actualy avaiable: Italian, English, Chinease 
- Improvement of notifications
- New features
- OTA updates
- Better performance

## New features

New features currently available are:

- StratOS menu (Option -> StratOS)
- Change language from the watch
- Applist options
- Volume Boost options
- Vibration at unlock options
- Switch Km/mi options
- Change font options (small, standard, big, biggest)
- QR code options (it can only be used for bind with android)
- Widget options (You can hide or view individual widgets directly from the watch)
- Local update (the ROM can be updated by copying the update.zip file)
- Task manager
- System Info
